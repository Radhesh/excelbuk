from django.db import models
from django import forms
from register.models import User
# Create your models here.

class Events(models.Model):
    event_type = models.CharField(primary_key = True, max_length = 30, blank =False, )
    class Meta:
        db_table = 'excelbuk_event'
    
    def __unicode__(self):
        return self.event_type


class User_Event(models.Model):
    email = models.ForeignKey(User, blank = False,db_column = 'email')
    event_type = models.ForeignKey(Events, blank = False, db_column = 'event_type')
    class Meta:
        db_table = 'excelbuk_user_event'
  #  def __unicode__(self):
  #      return self.user_event # doubt about return statement



class Event_Quiz(models.Model):
    event_name = models.CharField(primary_key = True, max_length = 100, blank =False)
   # event_type = models.ForeignKey(Events, max_length = 30, blank =False,db_column = 'event_type')
    organiser = models.CharField(max_length = 100, blank = False)
    location = models.CharField(max_length = 100, blank = False)
    date = models.DateField()
    description = models.CharField(max_length = 100, blank = False)
    class Meta:
        db_table = 'excelbuk_event_quiz'
    def __unicode__(self):
        return self.event_name

class Event_CTF(models.Model):
    event_name = models.CharField(primary_key = True, max_length = 100, blank =False)
   # event_type = models.ForeignKey(Events, max_length = 30, blank =False,db_column = 'event_type')
    organiser = models.CharField(max_length = 100, blank = False)
    location = models.CharField(max_length = 100, blank = False)
    date = models.DateField()
    description = models.CharField(max_length = 100, blank = False)
    class Meta:
        db_table = 'excelbuk_event_ctf'
    def __unicode__(self):
        return self.event_name

