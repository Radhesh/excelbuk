from django.db import models
from django import forms

class User(models.Model):
    name = models.CharField(max_length=100,blank=False, unique=True)
    date_of_birth = models.DateField()
    gender = models.CharField(max_length=100,blank=False)
    contact_no = models.CharField(max_length=11)
    organisation = models.CharField(max_length=150, blank=False)
    email = models.EmailField(blank=False, unique=True, primary_key=True)
    isactive = models.BooleanField(default=False)
    class Meta:
        db_table= 'excelbuk_user'

    def __unicode__(self):
                return self.email

    def get_absolute_url(self):
                return "/users/%s" % self.username



class Login(models.Model):
    username = models.CharField(max_length=100, primary_key=True,
                                blank=False,unique=True)
    password = models.CharField(max_length=100, blank=False)
    email = models.ForeignKey(User, blank=False, db_column='email')
    class Meta:
        db_table= 'excelbuk_login'

    def __unicode__(self):
        return self.username

    def get_absolute_url(self):
        return "/users/%s" % self.username


# Create your models here.
