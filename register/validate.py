
from django.core.validators import validate_email
from django.core.exceptions import ValidationError

#Function to validate fields
def text_validate(string):
    try:
	if string.isalpha():
	    return True
    except :
	    return False

def alphnum_validate(string):
    try:
	if string.isalnum():
	    return True
    except :
	    return False

def num_validate(digit):
    try:
	if digit.isdigit():
	    print len(digit)
	    if (len(digit)==10):
	    	return True
	    else:
		return False
    except :
	    return False

def email_validate(email):
    try:
        validate_email(email)
        return True
    except :
        return False


