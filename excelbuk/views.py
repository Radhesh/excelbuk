from django.http import HttpResponse

import httplib2
import urllib

from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.template.loader import get_template
from django.core.context_processors import csrf

from django.conf import settings

from django.http import Http404

from register.models import *


def isloggedin(request):
    try:
       if request.session['loggedinside']:
          return True
       else:
          return False
    except KeyError:
       return False

def home(request):
    #fetch the detils from the database create the feed stream and redirect to home page
    return render_to_response('general/home.html',{'csrf_token' : csrf(request), 'username' : "request.session['username']" } ,RequestContext(request))
    

def logout(request):
    try:
        user = User.objects.get(name=request.session['username'])
        user.isactive = False
        user.save()
        del request.session['loggedinside']
        del request.session['username']

        request.session.flush()
    except KeyError:
        pass
    return HttpResponseRedirect("/login")



def login(request):
    if request.method != "POST":
        if isloggedin(request):
            return home(request)#render_to_response('general/home.html',{'csrf_token' : csrf(request), 'username' : request.session['username'] } ,RequestContext(request))
        else: 
            return render_to_response('login/login.html',{},RequestContext(request))

    #TODO Needed ???
    request.session.set_test_cookie()
    if request.session.test_cookie_worked():
        request.session.delete_test_cookie()
    else:
        return HttpResponse("Please enable cookies and try again")

    #Extract Input from request
    input_uname = str(request.POST['uname'])
    input_pwd = str(request.POST['pwd'])

    #Check in database for authentication
    login_tuple = Login.objects.all().filter(username=input_uname)

    error_msg = ""
    if login_tuple:
        actual_pwd = str(login_tuple[0].password)
        #check with hashed password #TODO anand and rakesh
        if input_pwd == actual_pwd:
            #Setting session 
            request.session['loggedinside'] = True
            request.session['username'] = input_uname
            request.session.set_expiry(0)
            request.session['loggedinside'] = True
            user = User.objects.get(name=input_uname)
            user.isactive = True
            user.save()
            
            #Redirect to home page, but Logged in
            return home(request)#render_to_response('general/home.html', { 'csrf_token' : csrf(request), 'username' : input_uname } ,RequestContext(request))
        else:
            error_msg = 'Please enter the right username/password combination'
    else:
        error_msg = 'Please enter the right username/password combination'

    return render_to_response('login/login.html',{'error_msg': error_msg},context_instance=RequestContext(request))

