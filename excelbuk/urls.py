from django.conf.urls import patterns, include, url

from django.contrib import admin
from views import *
from register.views import *
admin.autodiscover()


urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'excelbuk.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    (r'^login/$', login),
    (r'^home/$', home),
    (r'^addevent/$', include('manage_event.urls')),
    (r'^register/feed/$', feed),
    (r'^register/$', include('register.urls')),
    (r'^logout/$', logout),

)
